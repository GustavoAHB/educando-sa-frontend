import Vue from 'vue';
import Vuex from 'vuex';
import usuarios from './modules/usuarios';
import solicitudes from './modules/solicitudes';
import ordenes from './modules/ordenes';
import productos from './modules/productos';
import salidasAlmacen from './modules/salidasAlmacen';
import centroCostos from './modules/centroCostos';
import entradas from './modules/entradas';
import roles from './modules/roles';
import proveedores from './modules/proveedores';
import items from './modules/items';
import compras from './modules/compras';
import itemsSalida from './modules/itemsSalida';
import kardex from './modules/kardex';
import itemsRecibidos from './modules/itemsRecibidos';

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
  modules: {
    usuarios,
    solicitudes,
    ordenes,
    productos,
    salidasAlmacen,
    centroCostos,
    entradas,
    roles,
    proveedores,
    items,
    compras,
    itemsSalida,
    itemsRecibidos,
    kardex,
  },
  strict: debug,
});
