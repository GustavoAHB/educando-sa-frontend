import centroCostos from '@/api/educando/centroCostos';

const state = {
  lista: [],
};

const getters = {};

const actions = {
  obtenerListaCentroCosto(context) {
    return centroCostos.listaCentroCostos().then((response) => {
      console.log(response);
      context.commit('SET_CENTROCOSTOS', response.data);
    });
  },
  crearCentroCosto(context, nuevoCentroCosto) {
    return centroCostos.crearCentroCostos(nuevoCentroCosto).then((response) => {
      context.commit("NUEVO_CENTROCOSTOS", response.data.centroCosto);
      return response.data;
    });
  },
  editarCentroCosto(context, centroCosto) {
    return centroCostos.editarCentroCostos(centroCosto).then((response) => {
      context.commit("EDITAR_CENTROCOSTOS", centroCosto);
      return response.data;
    });
  },
  eliminarCentroCosto(context, centroCosto) {
    return centroCostos.eliminarCentroCostos(centroCosto).then((response) => {
      context.commit("ELIMINAR_CENTROCOSTOS", centroCosto);
      return response.data;
    });
  },
};

const mutations = {
  SET_CENTROCOSTOS(state, payload) {
    state.lista = payload;
  },
  NUEVO_CENTROCOSTOS(state, payload) {
    state.lista.push(payload);
  },
  EDITAR_CENTROCOSTOS(state, payload) {
    state.lista[
      state.lista.findIndex((producto) => producto.idcentrocosto === payload.idcentrocosto)
    ] = payload;
  },
  ELIMINAR_CENTROCOSTOS(state, payload) {
    state.lista.splice(state.lista.indexOf(payload), 1);
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
