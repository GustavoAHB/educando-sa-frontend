import itemsSalida from "@/api/educando/itemsSalida";

const state = {
  lista: [],
};

const getters = {};

const actions = {
  obtenerListaItemSalida(context) {
    return itemsSalida.listaItemSalida().then((response) => {
      console.log(response);
      context.commit('SET_ITEMSALIDA', response.data);
    });
  },
  crearItemSalida(context, nuevoItemSalida) {
    return itemsSalida.crearItemSalida(nuevoItemSalida).then((response) => {
      context.commit("NUEVO_ITEM", response.data.item);
      return response.data;
    });
  },
};

const mutations = {
  SET_ITEMSALIDA(state, payload) {
    state.lista = payload;
  },
  NUEVO_ITEM(state, payload) {
    state.lista.push(payload);
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
