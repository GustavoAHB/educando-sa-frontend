import productos from "@/api/educando/productos";

const state = {
  lista: [],
};

const getters = {};

const actions = {
  obtenerListaProductos(context) {
    return productos.listaProductos().then((response) => {
      console.log(response);
      context.commit("SET_PRODUCTOS", response.data);
    });
  },
  crearProducto(context, nuevoProducto) {
    return productos.crearProducto(nuevoProducto).then((response) => {
      context.commit("NUEVO_PRODUCTO", response.data.producto);
      return response.data;
    });
  },
  editarProducto(context, producto) {
    return productos.editarProducto(producto).then((response) => {
      context.commit("EDITAR_PRODUCTO", producto);
      return response.data;
    });
  },
  eliminarProducto(context, producto) {
    return productos.eliminarProducto(producto).then((response) => {
      context.commit("ELIMINAR_PRODUCTO", producto);
      return response.data;
    });
  },
};

const mutations = {
  SET_PRODUCTOS(state, payload) {
    state.lista = payload;
  },
  NUEVO_PRODUCTO(state, payload) {
    state.lista.push(payload);
  },
  EDITAR_PRODUCTO(state, payload) {
    state.lista[
      state.lista.findIndex((producto) => producto.idproducto === payload.idproducto)
    ] = payload;
  },
  ELIMINAR_PRODUCTO(state, payload) {
    state.lista.splice(state.lista.indexOf(payload), 1);
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
