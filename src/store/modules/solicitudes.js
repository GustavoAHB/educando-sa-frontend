import solicitudes from '@/api/educando/solicitudes';

const state = {
  lista: [],
};

const getters = {};

const actions = {
  obtenerListaSolicitudes(context) {
    return solicitudes.listaSolicitudes().then((response) => {
      console.log(response);
      context.commit('SET_SOLICITUDES', response.data);
    });
  },
  crearSolicitud(context, nuevaSolicitud) {
    return solicitudes.crearSolicitud(nuevaSolicitud).then((response) => {
      context.commit("NUEVA_SOLICITUD", response.data.solicitud);
      return response.data;
    });
  },
  editarSolicitud(context, solicitud) {
    return solicitudes.editarSolicitud(solicitud).then((response) => {
      context.commit("EDITAR_SOLICITUD", solicitud);
      return response.data;
    });
  },
};

const mutations = {
  SET_SOLICITUDES(state, payload) {
    state.lista = payload;
  },
  NUEVA_SOLICITUD(state, payload) {
    state.lista.push(payload);
  },
  EDITAR_SOLICITUD(state, payload) {
    state.lista[
      state.lista.findIndex((solicitud) => solicitud.numsolictud === payload.numsolictud)
    ] = payload;
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
