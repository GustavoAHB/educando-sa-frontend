import kardex from "@/api/educando/kardex";

const state = {
  lista: [],
};

const getters = {};

const actions = {
  obtenerListaKardex(context) {
    return kardex.listaKardex().then((response) => {
      console.log(response);
      context.commit('SET_KARDEX', response.data);
    });
  },
};

const mutations = {
  SET_KARDEX(state, payload) {
    state.lista = payload;
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
