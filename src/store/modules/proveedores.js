import proveedores from "@/api/educando/proveedores";

const state = {
  lista: [],
};

const getters = {};

const actions = {
  obtenerListaProveedores(context) {
    return proveedores.listaProveedor().then((response) => {
      console.log(response);
      context.commit("SET_PROVEEDOR", response.data);
    });
  },
  crearProveedor(context, nuevoProveedor) {
    return proveedores.crearProveedor(nuevoProveedor).then((response) => {
      context.commit("NUEVO_PROVEEDOR", response.data.proveedor);
      return response.data;
    });
  },
  editarProveedor(context, proveedor) {
    return proveedores.editarProveedor(proveedor).then((response) => {
      context.commit("EDITAR_PROVEEDOR", proveedor);
      return response.data;
    });
  },
  eliminarProveedor(context, proveedor) {
    return proveedores.eliminarProveedor(proveedor).then((response) => {
      context.commit("ELIMINAR_PROVEEDOR", proveedor);
      return response.data;
    });
  },
};

const mutations = {
  SET_PROVEEDOR(state, payload) {
    state.lista = payload;
  },
  NUEVO_PROVEEDOR(state, payload) {
    state.lista.push(payload);
  },
  EDITAR_PROVEEDOR(state, payload) {
    state.lista[
      state.lista.findIndex((proveedor) => proveedor.nit === payload.nit)
    ] = payload;
  },
  ELIMINAR_PROVEEDOR(state, payload) {
    state.lista.splice(state.lista.indexOf(payload), 1);
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
