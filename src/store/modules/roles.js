import roles from '@/api/educando/roles';

const state = {
  lista: [],
};

const getters = {};

const actions = {
  obtenerListaRoles(context) {
    return roles.listaRoles().then((response) => {
      console.log(response);
      context.commit('SET_ROLES', response.data);
    });
  },
  crearRol(context, nuevoRol) {
    return roles.crearRol(nuevoRol).then((response) => {
      context.commit("NUEVO_ROL", response.data.rol);
      return response.data;
    });
  },
  editarRol(context, rol) {
    return roles.editarRol(rol).then((response) => {
      context.commit("EDITAR_ROL", rol);
      return response.data;
    });
  },
  eliminarRol(context, rol) {
    return roles.eliminarRol(rol).then((response) => {
      context.commit("ELIMINAR_ROL", rol);
      return response.data;
    });
  },
};

const mutations = {
  SET_ROLES(state, payload) {
    state.lista = payload;
  },
  NUEVO_ROL(state, payload) {
    state.lista.push(payload);
  },
  EDITAR_ROL(state, payload) {
    state.lista[
      state.lista.findIndex((rol) => rol.idrol === payload.idrol)
    ] = payload;
  },
  ELIMINAR_ROL(state, payload) {
    state.lista.splice(state.lista.indexOf(payload), 1);
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
