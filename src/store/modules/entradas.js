import entradas from '@/api/educando/entradaAlmacen';

const state = {
  lista: [],
};

const getters = {};

const actions = {
  obtenerListaEntradas(context) {
    return entradas.listaEntradasAlmacen().then((response) => {
      console.log(response);
      context.commit('SET_ENTRADASALMACEN', response.data);
    });
  },
  crearEntradasAlmacen(context, nuevaEntrada) {
    return entradas.crearEntradasAlmacen(nuevaEntrada).then((response) => {
      context.commit("NUEVA_ENTRADA", response.data.entrada);
      return response.data;
    });
  },
};

const mutations = {
  SET_ENTRADASALMACEN(state, payload) {
    state.lista = payload;
  },
  NUEVA_ENTRADA(state, payload) {
    state.lista.push(payload);
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
