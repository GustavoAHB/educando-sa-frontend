import ordenes from '@/api/educando/ordenes';

const state = {
  lista: [],
};

const getters = {};

const actions = {
  obtenerListaOrdenes(context) {
    return ordenes.listaOrdenes().then((response) => {
      console.log(response);
      context.commit('SET_ORDENES', response.data);
    });
  },
  editarOrden(context, orden) {
    return ordenes.editarOrden(orden).then((response) => {
      context.commit("EDITAR_ORDEN", orden);
      return response.data;
    });
  },
  crearOrden(context, nuevaOrden) {
    return ordenes.crearOrden(nuevaOrden).then((response) => {
      context.commit("NUEVA_ORDEN", response.data.ordenes);
      return response.data;
    });
  },
};

const mutations = {
  SET_ORDENES(state, payload) {
    state.lista = payload;
  },
  EDITAR_ORDEN(state, payload) {
    state.lista[
      state.lista.findIndex((orden) => orden.numorden === payload.numorden)
    ] = payload;
  },
  NUEVA_ORDEN(state, payload) {
    state.lista.push(payload);
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
