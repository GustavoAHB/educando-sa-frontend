import compras from "@/api/educando/compras";

const state = {
  lista: [],
};

const getters = {};

const actions = {
  obtenerListaCompras(context) {
    return compras.listaCompras().then((response) => {
      console.log(response);
      context.commit('SET_COMPRAS', response.data);
    });
  },
  crearCompra(context, nuevaCompra) {
    return compras.crearCompra(nuevaCompra).then((response) => {
      context.commit("NUEVO_COMPRAS", response.data.item);
      return response.data;
    });
  },
};

const mutations = {
  SET_COMPRAS(state, payload) {
    state.lista = payload;
  },
  NUEVO_COMPRAS(state, payload) {
    state.lista.push(payload);
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
