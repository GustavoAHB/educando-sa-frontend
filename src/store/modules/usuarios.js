import usuarios from "@/api/educando/usuarios";
import roles from "@/api/educando/roles";
import centrocostos from "@/api/educando/centroCostos";

const state = {
  lista: [],
  listaRoles: [],
  listaCentroCostos: [],
};

const getters = {};

const actions = {
  obtenerListaUsuarios(context) {
    return usuarios.listaUsuarios().then((response) => {
      console.log(response);
      context.commit("SET_USUARIOS", response.data);
    });
  },
  crearUsuario(context, nuevoUsuario) {
    return usuarios.crearUsuario(nuevoUsuario).then((response) => {
      context.commit("NUEVO_USUARIO", response.data.usuario);
      return response.data;
    });
  },
  editarUsuario(context, usuario) {
    return usuarios.editarUsuario(usuario).then((response) => {
      context.commit("EDITAR_USUARIO", usuario);
      return response.data;
    });
  },
  eliminarUsuario(context, usuario) {
    return usuarios.eliminarUsuario(usuario).then((response) => {
      context.commit("ELIMINAR_USUARIO", usuario);
      return response.data;
    });
  },
  login(context, usuario) {
    return usuarios.login(usuario).then((response) => {
      context.commit("LOGIN", response.data.usuario);
      return response.data;
    });
  },
  obtenerListaRoles(context) {
    return roles.listaRoles().then((response) => {
      console.log(response);
      context.commit("SET_ROLES", response.data);
    });
  },
  obtenerListaCentroCostos(context) {
    return centrocostos.listaCentroCostos().then((response) => {
      console.log(response);
      context.commit("SET_CENTROCOSTOS", response.data);
    });
  },
};

const mutations = {
  SET_USUARIOS(state, payload) {
    state.lista = payload;
  },
  NUEVO_USUARIO(state, payload) {
    state.lista.push(payload);
  },
  EDITAR_USUARIO(state, payload) {
    state.lista[
      state.lista.findIndex((usuario) => usuario.idusuario === payload.idusuario)
    ] = payload;
  },
  ELIMINAR_USUARIO(state, payload) {
    state.lista.splice(state.lista.indexOf(payload), 1);
  },
  LOGIN(state, payload) {
    state.lista.push(payload);
  },
  SET_ROLES(state, payload) {
    state.listaRoles = payload;
  },
  SET_CENTROCOSTOS(state, payload) {
    state.listaCentroCostos = payload;
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
