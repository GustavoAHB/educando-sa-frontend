import items from "@/api/educando/items";

const state = {
  lista: [],
};

const getters = {};

const actions = {
  obtenerListaItems(context) {
    return items.listaItems().then((response) => {
      console.log(response);
      context.commit('SET_ITEM', response.data);
    });
  },
  crearItem(context, nuevoItem) {
    return items.crearItem(nuevoItem).then((response) => {
      context.commit("NUEVO_ITEM", response.data.item);
      return response.data;
    });
  },
};

const mutations = {
  SET_ITEM(state, payload) {
    state.lista = payload;
  },
  NUEVO_ITEM(state, payload) {
    state.lista.push(payload);
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
