import itemsRecibidos from "@/api/educando/itemsRecibidos";

const state = {
  lista: [],
};

const getters = {};

const actions = {
  obtenerListaItemRecibido(context) {
    return itemsRecibidos.listaItemsRecibidos().then((response) => {
      console.log(response);
      context.commit('SET_ITEMRECIBIDO', response.data);
    });
  },
  crearItemRecibido(context, nuevoItem) {
    return itemsRecibidos.crearItemRecibido(nuevoItem).then((response) => {
      context.commit("NUEVO_ITEMRECIBIDO", response.data.item);
      return response.data;
    });
  },
};

const mutations = {
  SET_ITEMRECIBIDO(state, payload) {
    state.lista = payload;
  },
  NUEVO_ITEMRECIBIDO(state, payload) {
    state.lista.push(payload);
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
