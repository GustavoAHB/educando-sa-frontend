import salidasAlmacen from "@/api/educando/salidasAlmacen";

const state = {
  lista: [],
};

const getters = {};

const actions = {
  obtenerListaSalidaAlmacen(context) {
    return salidasAlmacen.listaSalidasAlmacen().then((response) => {
      console.log(response);
      context.commit("SET_SALIDA", response.data);
    });
  },
  crearSalidaAlmacen(context, nuevaSalida) {
    return salidasAlmacen.crearSalidaAlmacen(nuevaSalida).then((response) => {
      context.commit("NUEVO_SALIDA", response.data.salidaAlmacen);
      return response.data;
    });
  },
  editarSalidaAlmacen(context, salidaAlmacen) {
    return salidasAlmacen.editarSalidaAlmacen(salidaAlmacen).then((response) => {
      context.commit("EDITAR_SALIDA", salidaAlmacen);
      return response.data;
    });
  },
  eliminarSalidaAlmacen(context, salidaAlmacen) {
    return salidasAlmacen.eliminarUsuario(salidaAlmacen).then((response) => {
      context.commit("ELIMINAR_SALIDA", salidaAlmacen);
      return response.data;
    });
  },
};

const mutations = {
  SET_SALIDA(state, payload) {
    state.lista = payload;
  },
  NUEVO_SALIDA(state, payload) {
    state.lista.push(payload);
  },
  EDITAR_SALIDA(state, payload) {
    state.lista[
      state.lista.findIndex((salidasAlmacen) => salidasAlmacen.numsalida === payload.numsalida)
    ] = payload;
  },
  ELIMINAR_SALIDA(state, payload) {
    state.lista.splice(state.lista.indexOf(payload), 1);
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
