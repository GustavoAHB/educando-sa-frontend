import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';

Vue.use(VueRouter);

const routes = [{
  path: '/',
  name: 'Home',
  component: Home,
},
{
  path: '/about',
  name: 'About',
  component: () => import(/* webpackChunkName: "about" */ '../views/About.vue'),
},
{
  path: '/dashboard',
  name: 'Dashboard',
  component: () => import('../views/layout/Dashboard.vue'),
  children: [{
    path: "/usuarios",
    name: "Usuarios",
    component: () => import("@/views/usuarios/Index.vue"),
    meta: {
      jefeArea: true,
    },
  },
  {
    path: "/solicitudes",
    name: "solicitudes",
    component: () => import("@/views/solicitudes/Index.vue"),
    meta: {
      jefeArea: true,
    },
  },
  {
    path: "/solicitudesev",
    name: "Solicitudes",
    component: () => import("@/views/solicitudes/solicitudesAC/Index.vue"),
    meta: {
      jefeArea: true,
    },
  },
  {
    path: "/solicitudesrechazadas",
    name: "Rechazadas",
    component: () => import("@/views/solicitudes/solRechazadas/Index.vue"),
    meta: {
      jefeArea: true,
    },
  },
  {
    path: "/solicitudesaceptadas",
    name: "Aceptados",
    component: () => import("@/views/solicitudes/solAceptadas/Index.vue"),
  },
  {
    path: "/productos",
    name: "Productos",
    component: () => import("@/views/productos/Index.vue"),
  },
  {
    path: "/centroCostos",
    name: "CentroCostos",
    component: () => import("@/views/centroCostos/Index.vue"),
    meta: {
      jefeArea: true,
    },
  },
  // ADMINISTRADOR DE ALMACÉN
  {
    path: "/salidaAlmacen",
    name: "SalidaAlmacen",
    component: () => import("@/views/salidasAlmacen/Index.vue"),
    meta: {
      administradorAlmacen: true,
    },
  },
  {
    path: "/entradasalmacen",
    name: "Entradas",
    component: () => import("@/views/entradas/Index.vue"),
    meta: {
      administradorAlmacen: true,
    },
  },
  // ORDENES CONTRACTUALES
  {
    path: "/ordenescontractuales",
    name: "Ordenes",
    component: () => import("@/views/ordenes/Index.vue"),
    meta: {
      jefeArea: true,
    },
  },
  {
    path: "/ordenescontractualesev",
    name: "Ordenes",
    component: () => import("@/views/ordenes/ordenesDF/Index.vue"),
    meta: {
      jefeArea: true,
    },
  },
  {
    path: "/ordenescontractualesaceptadas",
    name: "OrdenesAceptadas",
    component: () => import("@/views/ordenes/ordenesAceptadas/Index.vue"),
    meta: {
      jefeArea: true,
    },
  },
  {
    path: "/ordenescontractualesrechazadas",
    name: "OrdenesRechazadas",
    component: () => import("@/views/ordenes/ordenesRechazadas/Index.vue"),
    meta: {
      jefeArea: true,
    },
  },
  {
    path: "/solicitudesrechazadas",
    name: "Rechazadas",
    component: () => import("@/views/solicitudes/solRechazadas/Index.vue"),
    meta: {
      jefeArea: true,
    },
  },
  {
    path: "/solicitudesaceptadas",
    name: "Aceptados",
    component: () => import("@/views/solicitudes/solAceptadas/Index.vue"),
  },
  {
    path: "/proveedores",
    name: "Proveedor",
    component: () => import("@/views/proveedores/Index.vue"),
    meta: {
      jefeArea: true,
    },
  },
  // DIRECTOR FINANCIERO
  {
    path: "/ordenescontractuales",
    name: "Ordenes",
    component: () => import("@/views/ordenes/Index.vue"),
    meta: {
      jefeArea: true,
    },
  },
  {
    path: "/ordenescontractualesaceptadas",
    name: "OrdenesAceptadas",
    component: () => import("@/views/ordenes/ordenesAceptadas/Index.vue"),
    meta: {
      jefeArea: true,
    },
  },
  {
    path: "/ordenescontractualesrechazadas",
    name: "OrdenesRechazadas",
    component: () => import("@/views/ordenes/ordenesRechazadas/Index.vue"),
    meta: {
      jefeArea: true,
    },
  },
  // ADMINISTRADOR DE INVENTARIO
  {
    path: "/kardex",
    name: "Kardex",
    component: () => import("@/views/kardex/Index.vue"),
    meta: {
      administradorAlmacen: true,
      adiministradorInventario: true,
    },
  },
  {
    path: "/inventario",
    name: "Inventario",
    component: () => import("@/views/inventario/Index.vue"),
    meta: {
      jefeArea: true,
    },
  },
  ],
},
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
