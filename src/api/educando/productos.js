import axios from 'axios';
import API_URL from '@/api';

const listaProductos = () => axios.get(`${API_URL}/productos`);
const crearProducto = (nuevoProducto) => axios.post(`${API_URL}/productos`, nuevoProducto);
const editarProducto = (producto) => axios.put(`${API_URL}/productos`, producto);
const eliminarProducto = (producto) => axios.delete(`${API_URL}/productos`, { params: producto });

export default {
  listaProductos,
  crearProducto,
  editarProducto,
  eliminarProducto,
};
