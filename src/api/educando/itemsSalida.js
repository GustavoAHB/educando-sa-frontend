import axios from 'axios';
import API_URL from '@/api';

const listaItemSalida = () => axios.get(`${API_URL}/itemssalida`);
const crearItemSalida = (nuevoItem) => axios.post(`${API_URL}/itemssalida`, nuevoItem);

export default {
  crearItemSalida,
  listaItemSalida,
};
