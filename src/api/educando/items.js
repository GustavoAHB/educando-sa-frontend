import axios from 'axios';
import API_URL from '@/api';

const listaItems = () => axios.get(`${API_URL}/items`);
const crearItem = (nuevoItem) => axios.post(`${API_URL}/items`, nuevoItem);

export default {
  crearItem,
  listaItems,
};
