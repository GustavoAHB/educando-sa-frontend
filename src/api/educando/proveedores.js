import axios from 'axios';
import API_URL from '@/api';

const listaProveedor = () => axios.get(`${API_URL}/proveedores`);
const crearProveedor = (nuevoProveedor) => axios.post(`${API_URL}/proveedores`, nuevoProveedor);
const editarProveedor = (proveedor) => axios.put(`${API_URL}/proveedores`, proveedor);
const eliminarProveedor = (proveedor) => axios.delete(`${API_URL}/proveedores`, { params: proveedor });

export default {
  listaProveedor,
  crearProveedor,
  editarProveedor,
  eliminarProveedor,
};
