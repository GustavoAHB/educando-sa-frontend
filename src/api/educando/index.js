import usuarios from './usuarios';
import solicitudes from './solicitudes';
import ordenes from './ordenes';
import centroCostos from './centroCostos';
import productos from './productos';
import entradasAlmacen from './entradaAlmacen';
import salidasAlmacen from './salidasAlmacen';
import roles from './roles';
import proveedores from './proveedores';
import items from './items';
import compras from './compras';
import itemsRecibidos from './itemsRecibidos';
import itemSalida from './itemsSalida';

export default {
  usuarios,
  solicitudes,
  ordenes,
  centroCostos,
  entradasAlmacen,
  salidasAlmacen,
  productos,
  roles,
  proveedores,
  items,
  compras,
  itemsRecibidos,
  itemSalida,
};
