import axios from 'axios';
import API_URL from '@/api';

const listaItemsRecibidos = () => axios.get(`${API_URL}/itemsrecibidos`);
const crearItemRecibido = (nuevoItem) => axios.post(`${API_URL}/itemsrecibidos`, nuevoItem);

export default {
  crearItemRecibido,
  listaItemsRecibidos,
};
