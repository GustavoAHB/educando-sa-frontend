import axios from 'axios';
import API_URL from '@/api';

const listaOrdenes = () => axios.get(`${API_URL}/ordenescontractuales`);
const crearOrden = (nuevaOrden) => axios.post(`${API_URL}/ordenescontractuales`, nuevaOrden);
const editarOrden = (orden) => axios.put(`${API_URL}/ordenescontractuales`, orden);
const eliminarOrden = (orden) => axios.delete(`${API_URL}/ordenescontractuales`, { params: orden });

export default {
  listaOrdenes,
  crearOrden,
  editarOrden,
  eliminarOrden,
};
