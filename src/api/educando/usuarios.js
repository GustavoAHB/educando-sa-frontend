import axios from 'axios';
import API_URL from '@/api';

const listaUsuarios = () => axios.get(`${API_URL}/usuarios`);
const crearUsuario = (nuevoUsuario) => axios.post(`${API_URL}/usuarios`, nuevoUsuario);
const editarUsuario = (usuario) => axios.put(`${API_URL}/usuarios`, usuario);
const eliminarUsuario = (usuario) => axios.delete(`${API_URL}/usuarios`, { params: usuario });
const login = (usuario) => axios.post(`${API_URL}/auth/login`, usuario);

export default {
  listaUsuarios,
  crearUsuario,
  editarUsuario,
  eliminarUsuario,
  login,
};
