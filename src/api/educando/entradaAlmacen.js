import axios from 'axios';
import API_URL from '@/api';

const listaEntradasAlmacen = () => axios.get(`${API_URL}/entradasalmacen`);
const crearEntradasAlmacen = (nuevaEntrada) => axios.post(`${API_URL}/entradasalmacen`, nuevaEntrada);
const editarEntradasAlmacen = (entrada) => axios.put(`${API_URL}/entradasalmacen`, entrada);
const eliminarEntradasAlmacen = (entrada) => axios.delete(`${API_URL}/entradasalmacen`, { params: entrada });

export default {
  listaEntradasAlmacen,
  crearEntradasAlmacen,
  editarEntradasAlmacen,
  eliminarEntradasAlmacen,
};
