import axios from 'axios';
import API_URL from '@/api';

const listaSolicitudes = () => axios.get(`${API_URL}/solicitudescompras`);
const crearSolicitud = (nuevaSolicitud) => axios.post(`${API_URL}/solicitudescompras`, nuevaSolicitud);
const editarSolicitud = (solicitud) => axios.put(`${API_URL}/solicitudescompras`, solicitud);
const eliminarSolicitud = (solicitud) => axios.delete(`${API_URL}/solicitudescompras`, { params: solicitud });

export default {
  listaSolicitudes,
  crearSolicitud,
  editarSolicitud,
  eliminarSolicitud,
};
