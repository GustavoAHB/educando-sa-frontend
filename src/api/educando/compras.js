import axios from 'axios';
import API_URL from '@/api';

const listaCompras = () => axios.get(`${API_URL}/compras`);
const crearCompra = (nuevaCompra) => axios.post(`${API_URL}/compras`, nuevaCompra);

export default {
  listaCompras,
  crearCompra,
};
