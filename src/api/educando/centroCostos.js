import axios from 'axios';
import API_URL from '@/api';

const listaCentroCostos = () => axios.get(`${API_URL}/centrocostos`);
const crearCentroCostos = (nuevoCentroCosto) => axios.post(`${API_URL}/centrocostos`, nuevoCentroCosto);
const editarCentroCostos = (centroCosto) => axios.put(`${API_URL}/centrocostos`, centroCosto);
const eliminarCentroCostos = (centroCosto) => axios.delete(`${API_URL}/centrocostos`, { params: centroCosto });

export default {
  listaCentroCostos,
  crearCentroCostos,
  editarCentroCostos,
  eliminarCentroCostos,
};
