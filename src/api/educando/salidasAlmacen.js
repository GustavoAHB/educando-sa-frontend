import axios from 'axios';
import API_URL from '@/api';

const listaSalidasAlmacen = () => axios.get(`${API_URL}/salidasalmacen`);
const crearSalidaAlmacen = (nuevoSalidaAlmacen) => axios.post(`${API_URL}/salidasalmacen`, nuevoSalidaAlmacen);
const editarSalidaAlmacen = (salidaAlmacen) => axios.put(`${API_URL}/salidasalmacen`, salidaAlmacen);

export default {
  listaSalidasAlmacen,
  crearSalidaAlmacen,
  editarSalidaAlmacen,
};
