import axios from 'axios';
import API_URL from '@/api';

const listaRoles = () => axios.get(`${API_URL}/roles`);
const crearRol = (nuevoRol) => axios.post(`${API_URL}/roles`, nuevoRol);
const editarRol = (rol) => axios.put(`${API_URL}/roles`, rol);
const eliminarRol = (rol) => axios.delete(`${API_URL}/roles`, { params: rol });

export default {
  listaRoles,
  crearRol,
  editarRol,
  eliminarRol,
};
